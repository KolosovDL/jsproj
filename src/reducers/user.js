import {
    USER_INPUT_LOGIN_CHANGED,
    USER_TRY_TO_LOG_IN,
    GET_USERS_SUCCESS,
    GET_USERS_FAILED,
    GET_USERS_LOADING,
    USER_INPUT_PASS_CHANGED,
    USER_LOGOUT,
    USER_REGISTRATION,
    REG_INPUT_USERNAME_CHANGED,
    REG_INPUT_EMAIL_CHANGED,
    REG_CLOSE,
    REG_OPEN,
    GET_POSTS_FAILED,
    GET_POSTS_LOADING,
    GET_POSTS_SUCCESS,
    POST_INPUT_HEADER_CHANGED,
    POST_INPUT_TEXT_CHANGED,
    POST_CREATE,

} from '../constants';
import {Form} from "react-bootstrap";
import React from "react";

const initialState = {
  users: [],
  isUsersLoading: false,
  userLogin: '',
    userPassword: null,
  isLoggedIn: false,
  errMsg: '',
    errLogin: '',
  pages: [
    { pageId: 0, name: 'Главная', path: '/' },
    { pageId: 1, name: 'Пользователи', path: '/users' },
    { pageId: 2, name: 'Статьи', path: '/posts' },
    { pageId: 3, name: 'Написать статью', path: '/create' }
  ],

    RegistrationUserName:'',
    RegistrationEmail:'',
    RegistrationErr:'',
    show:false,
    posts: [
        { Id: 0, user: 'Admin', header:'HEADER',text: 'New stat' },
        { Id: 1, user: 'Admin', header:'HEADER',text: 'New stat1' },
        { Id: 2, user: 'Admin', header:'HEADER',text: 'New stat2' },
        { Id: 3, user: 'Admin', header:'HEADER',text: 'New stat3' },
    ],

    headerNewPost:'',
    textNewPost:''
};

export default function(state = initialState, action) {
  console.log('reducer');
  switch(action.type) {
    case USER_INPUT_LOGIN_CHANGED: {
      return {
        ...state,
        userLogin: action.value,
          errLogin:''
      };
    }
      case USER_INPUT_PASS_CHANGED: {
          return {
              ...state,
              userPassword: action.value,
              errLogin:''
          };
      }
    case USER_TRY_TO_LOG_IN: {
      const login = state.userLogin;
      const password = state.userPassword;
      const users=state.users;
      const findUer = () => {
          for (let user in users){

              if (login === users[user].username) return true
          }
      };
      if (findUer()){
          return {
              ...state,
              userLogin: login,
              isLoggedIn: password === 'qwerty'
          };
      }
      else
          return {
        ...state,
              errLogin: "Неверный пользователь или пароль",
      }
    }

      case USER_LOGOUT: {

          return {
              ...state,
              userLogin: null,
              isLoggedIn: false,
              errLogin: ''
          }
      }
      case USER_REGISTRATION: {
          const users=state.users;
          const RegistrationUserName=state.RegistrationUserName;
          const RegistrationEmail=state.RegistrationEmail;
          for (let user in users){

                  if (RegistrationUserName === users[user].username) return {
                      ...state,
                      RegistrationErr:'Такой пользователь уже есть'
                  }
              }

              const newUser={id:users.length+1,username:RegistrationUserName,email:RegistrationEmail};
         let newUsers = users.concat( newUser);
         alert("Вы в системе")
          return {
              ...state,
              users:newUsers,
              RegistrationUserName:'',
              RegistrationEmail:'',
              userLogin:RegistrationUserName,
              isLoggedIn:true,
              show: false,
          }
      }

    case GET_USERS_LOADING: {
      return {
        ...state,
        isUsersLoading: true
      };
    }

    case GET_USERS_SUCCESS: {
      return {
        ...state,
        users: action.data,
        isUsersLoading: false
      };
    }

    case GET_POSTS_FAILED: {
      return {
        ...state,
        isUsersLoading: false,
        errMsg: action.errMsg,
      };
    }


      case GET_POSTS_LOADING: {
          return {
              ...state,
              isUsersLoading: true
          };
      }

      case GET_POSTS_SUCCESS: {
          return {
              ...state,
              users: action.data,
              isUsersLoading: false
          };
      }

      case GET_USERS_FAILED: {
          return {
              ...state,
              isUsersLoading: false,
              errMsg: action.errMsg,
          };
      }

      case REG_INPUT_USERNAME_CHANGED: {
          return {

              ...state,
              RegistrationUserName: action.value,
              RegistrationErr:''
          };
      }

      case REG_INPUT_EMAIL_CHANGED: {
              return {

                  ...state,
                  RegistrationEmail: action.value,
                  RegistrationErr:''
              };
      }

case REG_CLOSE:{

    return {
        ...state,
        show:false
    }
}

      case REG_OPEN:{

          return {
              ...state,
              show:true
          }
      }

case POST_INPUT_HEADER_CHANGED: {
        return {
            ...state,
            headerNewPost: action.value,
        };}


    case POST_INPUT_TEXT_CHANGED: {
        return {
            ...state,
            textNewPost: action.value,
        };
    }


      case POST_CREATE: {

          const headerNewPost=state.headerNewPost;
          const textNewPost=state.textNewPost;
          const userLogin=state.userLogin;
          const posts=state.posts;

          const newPost={id:posts.length+1,user:userLogin,header:headerNewPost,text:textNewPost};
          let Posts = posts.concat(newPost);

          return {
              ...state,
              posts:Posts,
              headerNewPost:'',
              textNewPost:''

          }
      }
    default:
        return state;
    }

}