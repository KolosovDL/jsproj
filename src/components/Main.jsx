import React from 'react';
import Content from './Content';
import Header from './Header';
import UserContainer from './UserContainer';
import Article from './Article';
import Footer from './Footer';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import { Container} from 'react-bootstrap'
import Create from "./Create";

export default class Main extends React.Component {
  constructor(props) {
    super(props); // вызов конструктора класса React.Component
  }



  render() {
    return (


      <div className="content">
        <Router>
          <div>

            <Header />
              <Container>
            <Switch>
              <Route path="/" exact render={() => <Content />} />
              <Route path="/users" render={() => <UserContainer />} />
              <Route path="/posts" render={() => <Article /> } />
                <Route path="/create" render={() => <Create /> } />

            </Switch>
              </Container>
            <Footer />

          </div>
        </Router>
      </div>


    );
  }
}

// 'localhost:3000/about'
// const exampleRouting = (
//   <Switch>
//     <Route path="/about" component={About} />
//     <Route path="/:user" component={User} />
//     <Route component={NoMatch} />
//   </Switch>
// )