import React from 'react';
import { Link, Route } from 'react-router-dom';


export default function User(user) {
  return (
    <div>
      <p>{user.name}</p>
      <div>
        Контакты:
        <span>{user.email}</span>
        <span>{user.phone}</span>
      </div>

      <Route
        path={'/user/:id'}
        exact
        render={() => <Link to={'/users'}>Назад к списку</Link>}
      />

    </div>
  );
}