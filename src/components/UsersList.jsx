import React from 'react';
import User from './User';
import { Link } from 'react-router-dom';
export default class UsersList extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={'users'}>
        <h4>Пользователи</h4>
        <ol>
          {this.props.isUsersLoading && <span>Загрузка...</span>}
          {
            this.props.list.map((user, i) => {
              return (
                <div key={i}>
                  <User {...user} />
                  <Link to={'/user/' + user.id}>
                    Перейти в профиль
                  </Link>
                </div>
              );
            })
          }
        </ol>
      </div>
    )
  }
}