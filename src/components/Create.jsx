import React from 'react';
import User from './User';
import { Link } from 'react-router-dom';
import {bindActionCreators} from "redux";
import actions from "../actions/user";
import {connect} from "react-redux";
import {Button, Form, FormControl} from "react-bootstrap";
class Create extends React.Component {
    constructor(props) {
        super(props);
    }






    CreateForm(){

        return (
            <Form>
                <Form.Label>Название</Form.Label>
                <FormControl type="text" id={'header-input'}  ref={this.headerInput} onChange={(e) => {
                    this.props.actions.saveHeaderInputValue(e.target.value);
                }}/>

                <Form.Group controlId="exampleForm.ControlTextarea1">
                    <Form.Label>Содержимое:</Form.Label>
                    <Form.Control as="textarea" rows="3" id={'text-input'}  ref={this.textInput} onChange={(e) => {
                        this.props.actions.saveTextInputValue(e.target.value);
                    }}
                    />
                </Form.Group>

                <Button variant="outline-dark"  onSubmit="window.location.reload();" onClick={() => {
                    this.props.actions.postCreate();}}>Создать</Button>
            </Form>
        )
    }


    render() {
        return (
            <div>
                {this.props.isLoggedIn ? this.CreateForm() : <span>Войдите, чтобы создать пост.</span>}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    posts:state.user.posts,
    isLoggedIn: state.user.isLoggedIn,
    pages: state.user.pages,
    users: state.user.users,
    errLogin: state.user.errLogin,
    RegistrationUserName: state.user.RegistrationUserName,
    show:false,
    headerNewPost:state.user.headerNewPost,
    textNewPost:state.user.textNewPost,
});


const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Create);