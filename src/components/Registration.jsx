import React from 'react';
import {Modal, Button, Form, FormControl} from 'react-bootstrap';
import './Header.css';
import {bindActionCreators} from "redux";
import actions from "../actions/user";
import {connect} from "react-redux";
class Registration extends React.Component {
  constructor(props) {
    super(props);
  }




  render() {
    return (
      <>
        <Button variant="outline-light"  onClick={this.props.actions.handleShow}>
          Регистрация
        </Button>
        <Modal show={this.props.show} onHide={this.props.actions.handleClose} >
          <Modal.Header closeButton>
            <Modal.Title>Регистрация</Modal.Title>
          </Modal.Header>
          <Modal.Body>Заполните для регистрации</Modal.Body>
            {this.props.RegistrationErr}
            <Form className={"FormReg"}>
                    <Form.Label>Username</Form.Label>
                    <Form.Control type="text" placeholder="Username" id={'username-input'}  ref={this.usernameInput}           onChange={(e) => {
                        this.props.actions.saveUserNameInputValue(e.target.value);}}/>

                <Form.Label>Email</Form.Label>
                <Form.Control type="email" placeholder="email"
                              id={'email-input'}  ref={this.emailInput}           onChange={(e) => {

                    this.props.actions.saveEmailInputValue(e.target.value);}}/>

            </Form>
          <Modal.Footer>
            <Button variant="primary"   onClick={() => {
                this.props.actions.Registration();
            }}>
              Зарегестрироваться
            </Button>

          </Modal.Footer>
        </Modal>
      </>
    );
  }
}
const mapStateToProps = state => ({
show:state.user.show,
    userLogin:state.user.userLogin,
    isLoggedIn: state.user.isLoggedIn,
    pages: state.user.pages,
    users: state.user.users,
    errLogin: state.user.errLogin,
    RegistrationUserName: state.user.RegistrationUserName,
    RegistrationEmail:state.user.RegistrationEmail,
    RegistrationErr:state.user.RegistrationErr,
});


const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Registration);