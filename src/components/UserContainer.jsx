import axios from 'axios';
import React from 'react';
import UsersList from './UsersList';
import User from './User';
import { connect } from 'react-redux';
import actions from '../actions/user';
import { bindActionCreators } from 'redux';
import { Route } from 'react-router-dom';

class UserContainer extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.actions.fetchUsers();
  }

  render() {
    return (
      <div>
        <Route
          path={'/users'}
          exact
          render={
            (props) =>
              <UsersList
                {...props}
                list={this.props.users}
                isUsersLoading={this.props.isUsersLoading}
              />
          }
        />
        <Route
          path={'/user/:id'}
          exact
          render={(props) => {
            const userId = +props.match.params.id;
            const selectedUser = this.props.users.find(user => user.id === userId);
            return <User {...selectedUser} />;
          }}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  isLoading: state.user.isUsersLoading,
  users: state.user.users,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(actions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(UserContainer);