import React from 'react';
import {bindActionCreators} from "redux";
import actions from "../actions/user";
import {connect} from "react-redux";
class Article extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div><h1>Статьи</h1>
            <div className="item">

                    {
                        this.props.posts.map((posts, i) => {
                            return ( <div key={i} className="section-inner">
                                <h3>{posts.header}</h3>
                                <h5>{posts.user}</h5>
                                    <p> {posts.text}</p>
                                </div>
                            );
                        })
                    }
            </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    posts:state.user.posts,
    isLoggedIn: state.user.isLoggedIn,
    pages: state.user.pages,
    users: state.user.users,
    errLogin: state.user.errLogin,
    RegistrationUserName: state.user.RegistrationUserName,
    show:false
});


const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Article);



