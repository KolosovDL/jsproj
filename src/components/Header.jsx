import React from 'react';
import './Header.css';

import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Button, Navbar,Nav,FormControl,Form,Row } from 'react-bootstrap'
import {bindActionCreators} from "redux";
import actions from "../actions/user";
import 'bootstrap/dist/css/bootstrap.css';
import Registration from "./Registration"
import '/home/di/jsrun/jscurs/08_React_Redux/node_modules/bootstrap/dist/css/bootstrap.css';
class Header extends React.Component {
  constructor(props) {
    super(props);
  }

    componentDidMount() {
        this.props.actions.fetchUsers();
        //this.props.actions.fetchPost();
    }


  getList() {
    return this.props.pages.map((obj, i) => {
      return (
        <Nav.Item key={i} classname="nav-item">
<Nav.Link>
          <Link to={obj.path}>{obj.name}</Link>
</Nav.Link>
        </Nav.Item>


      )
    });
  }


  userPP(){
      return (

      <Form inline>
          <span className={'error'}> {this.props.errLogin}</span>
          <FormControl type="text" placeholder="Логин" className="mr-sm-2"  id={'login-input'}  ref={this.loginInput}           onChange={(e) => {
              console.log('handler onChanger');
              this.props.actions.saveUserInputValue(e.target.value);
          }}/>
          <FormControl type="text" placeholder="Пароль" className="mr-sm-2"  id={'password-input'}  ref={this.passwordInput}           onChange={(e) => {
              console.log('handler onChanger');
              this.props.actions.savePasswordInputValue(e.target.value);
          }}/>
          <Button variant="outline-light"  onClick={() => {
              this.props.actions.onLogin();
          }}>Войти</Button>

          <Registration/>

      </Form>)
  }


  userBB(){

      return (
          <Form inline>
          <h3 className={'custom-user-login'}>Добро пожалывать,{this.props.userLogin}!</h3>

          <Button variant="outline-light"  onClick={() => {
          this.props.actions.onLogout();
      }}>Выйти</Button>
  </Form>


      )
  }
  render() {
    console.log('Header props', this.props);
    return (

         <div>
          <Navbar bg="primary" variant="dark">
              <Navbar.Brand href="">Project </Navbar.Brand>


              <Nav className="mr-auto">
          {this.getList()}

              </Nav>



              {this.props.isLoggedIn ?  this.userBB(): this.userPP()}

          </Navbar>


</div>
    )
  }
}

const mapStateToProps = state => ({
        userLogin:state.user.userLogin,
    isLoggedIn: state.user.isLoggedIn,
    pages: state.user.pages,
    users: state.user.users,
    errLogin: state.user.errLogin,
    RegistrationUserName: state.user.RegistrationUserName,
    show:false
});


const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);



