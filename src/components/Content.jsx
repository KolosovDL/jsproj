import React from 'react';
import { bindActionCreators } from 'redux';
import actions from '../actions/user';
import { connect } from 'react-redux';
import './Body.css';



class Content extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillReceiveProps(nextProps, nextContext) {

  }

  shouldComponentUpdate(nextProps, nextState, nextContext) {

    return true;
  }

  render() {

    console.log('Content props', this.props);
    return (
<div>Project</div>

    )
  }
}

const mapStateToProps = state => ({
...state
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(actions, dispatch)
});

const Wrapped = connect(mapStateToProps, mapDispatchToProps)(Content);
export default Wrapped;