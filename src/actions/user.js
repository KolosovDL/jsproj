import * as c from '../constants';
import axios from 'axios';

const actions = {
  saveUserInputValue(value) {
    console.log('action saveUserInputValue');
    return {
      value,
      type: c.USER_INPUT_LOGIN_CHANGED
    };
  },
    savePasswordInputValue(value) {
        console.log('action saveUserInputValue');
        return {
            value,
            type: c.USER_INPUT_PASS_CHANGED
        };
    },
  onLogin() {
      return {
          type: c.USER_TRY_TO_LOG_IN,
      };
  },
      onLogout(){
          return {
              type: c.USER_LOGOUT,
          };



  },
    saveUserNameInputValue(value) {

        return {
            value,
            type: c.REG_INPUT_USERNAME_CHANGED
        };},

    saveEmailInputValue(value) {

        return {
            value,
            type: c.REG_INPUT_EMAIL_CHANGED
        };},


    Registration(){
        return {
            type: c.USER_REGISTRATION,
        };


    },

    handleClose(){
        return {
            type: c.REG_CLOSE,
        };},

    handleShow(){
        return {
            type: c.REG_OPEN,
        };},

    fetchUsers() {
        const testAction = (dispatch, getStore) => {
            dispatch({
                type: c.GET_USERS_LOADING
            });

            const store = getStore();
            console.log('store', store);

            axios
                .get('https://jsonplaceholder.typicode.com/users')
                .then(response => {
                    dispatch({
                        type: c.GET_USERS_SUCCESS,
                        data: response.data,
                    });
                })
                .catch(err => {
                    dispatch({
                        type: c.GET_USERS_FAILED,
                        errMsg: err.message,
                    });
                });
        };

        return testAction;
    },

    saveHeaderInputValue(value) {
        console.log('action saveUserInputValue');
        return {
            value,
            type: c.POST_INPUT_HEADER_CHANGED
        };
    },
    saveTextInputValue(value) {
        console.log('action saveTextInputValue');
        return {
            value,
            type: c.POST_INPUT_TEXT_CHANGED
        };
    },

    postCreate() {
        console.log('action postCreate');
        return {
            type: c.POST_CREATE,
        };
    },



};

export default actions;
